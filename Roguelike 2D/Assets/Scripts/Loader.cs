﻿using System.Collections;
using UnityEngine;

public class Loader : MonoBehaviour
{
    public GameObject gameManager;
// public GameObject soundManager;
    // Start is called before the first frame update
    void Awake()
    {
        //Check if a GameManager has already been assigned to static variable GameManager.instance or if it's still null
        if (GameManager.instance == null)
            Instantiate(gameManager);
        //Check if a SoundManager has already been assigned to static variable GameManager.instance or if it's still null
//if (SoundManager.instance == null)
// Instantiate(soundManager);
    }

}
